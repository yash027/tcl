import { Injectable } from "@angular/core";
import { Network } from '@ionic-native/network/ngx';
import { ToastController } from "@ionic/angular";

@Injectable({
    providedIn: 'root'
})
export class NetworkService {

    constructor(private network: Network, private toastController: ToastController) {
        this.network.onConnect().subscribe( connected => {
            this.toastController.create(
                {
                    color: 'success',
                    message: 'Connected',
                    duration: 2000
                }
            ).then( toast => toast.present());
        });
        this.network.onDisconnect().subscribe( disconnected => {
            this.toastController.create(
                {
                    color: 'danger',
                    message: 'Not Connected To Internet, Please Check Your Connection',
                    duration: 2000
                }
            ).then( toast => toast.present());
        })
    }

}