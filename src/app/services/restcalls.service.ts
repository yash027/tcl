import { Injectable } from '@angular/core';
import {
  HttpClient,
} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestcallsService {

  baseUrl: string;
  
  constructor(private http: HttpClient) {}

  call_GET(url) {
    return this.http.get(this.baseUrl + '/api/' + url);
  }
  call_POST(url, data) {
    return this.http.post(this.baseUrl + '/api/' + url, data);
  }
  call_PUT(url, data) {
    return this.http.put(this.baseUrl + '/api/' + url, data);
  }
  call_DELETE(url) {
    return this.http.delete(this.baseUrl + '/api/' + url);
  }
}
