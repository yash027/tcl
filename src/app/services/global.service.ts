import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Plugins, CameraResultType, CameraSource, Capacitor } from '@capacitor/core';

const { Camera } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  colors: any = ['primary', 'success', 'secondary', 'tertiary', 'warning', 'danger', 'dark', 'medium'];

  constructor(private toastController: ToastController, private router: Router) { }

  getColorClass(index) {
    if (index >= this.colors.length) {
      while (index >= this.colors.length) {
        index = index - this.colors.length;
      }
    }
    return this.colors[index];
  }

  displayToastMessage(message) {
    this.toastController
      .create({
        message: message,
        duration: 1000
      })
      .then(toast => toast.present());
  }

  gotoLogin() {
    this.router.navigate(['login']);
  }

  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    let mimeString = dataURI
      .split(',')[0]
      .split(':')[1]
      .split(';')[0];

    // write the bytes of the string to a typed array
    let ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString});
  }

  getSAPDate(date) {
    let selectedDate: any = new Date(date);
    let month = selectedDate.getMonth();
    if (month === 9) {
      month = month + 1;
    }
    if (month < 9) {
      month = month + 1;
      month = '0' + month.toString();
    }
    let newDate = selectedDate.getDate();
    if (newDate <= 9) {
      newDate = '0' + newDate.toString();
    }
    return (selectedDate =
      selectedDate.getFullYear().toString() + month + newDate);
  }

  getPortalDate(date) {
    if(date) {
      const year = date.slice(0, 4);
      const month = date.slice(4, 6);
      const day = date.slice(6, 8);
      const newDate = day + '-' + month + '-' + year;
      return newDate;
    }
  }

  getRandomNumber() {
    return Math.floor(10000000 + Math.random() * 90000000);
  }

  getBytesConversion(a, b) {
    if (0 === a) {
      return '0 Bytes';
    }
    const c = 1024,
      d = b || 2,
      e = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      f = Math.floor(Math.log(a) / Math.log(c));
    return parseFloat((a / Math.pow(c, f)).toFixed(d)) + ' ' + e[f];
  }

  getAttachmentIcon(type) {
    let iconName;
    switch (type) {
      case 'jpg':
      case 'jpeg':
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'png': {
        iconName = 'image';
        break;
      }
      case 'html':
      case 'text/html': {
        iconName = 'logo-html5';
        break;
      }
      case 'css':
      case 'text/css': {
        iconName = 'logo-css3';
        break;
      }
      case 'txt':
      case 'text/plain': {
        iconName = 'text';
        break;
      }
      default: {
        iconName = 'document';
        break;
      }
    }
    return iconName;
  }

  hideBottomTabs() {
    const elem = <HTMLElement>document.querySelector('#tabs');
    if (elem != null) {
      elem.style.display = 'none';
    }
  }

  showBottomTabs() {
    const elem = <HTMLElement>document.querySelector('#tabs');
    if (elem != null) {
      elem.style.display = 'flex';
    }
  }
}
