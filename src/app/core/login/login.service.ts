import { Injectable } from '@angular/core';
import { RestcallsService } from '../../services/restcalls.service';
import { StorageService } from '../../services/storage.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: RestcallsService, private storage: StorageService, private httpClient: HttpClient) { }

  authenticateUser(userDetails: any) {
    userDetails['rememberMe'] = 'true';
    const url = '/authenticate';
    return this.http.call_POST(url, userDetails);
  }

  getAccountDetails() {
    const url = '/account';
    return this.http.call_GET(url);
  }

  getCompanyDetails(companyCode: any) {
    return this.httpClient.get('http://smartdocs-mobile-login.appspot.com/rest/api/getCompanyURL/' + companyCode);
  }

  saveUserData(user) {
    this.storage.saveUser(user);
    localStorage.setItem('role', user.authorities);
  }

  removeUserData() {
    this.storage.deleteUser();
    localStorage.clear();
  }
}
