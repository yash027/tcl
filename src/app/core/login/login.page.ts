import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { GlobalService } from '../../services/global.service';
import { Router } from '@angular/router';
import { LoadingController, Platform } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { HttpErrorResponse } from '@angular/common/http';
import { RestcallsService } from '../../services/restcalls.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  passwordType: any;

  isAccountIDVerified: boolean;
  companyCode: any;

  togglePasswordType() {
    this.passwordType = this.passwordType || 'password';
    this.passwordType = this.passwordType === 'password' ? 'text' : 'password';
  }

  data: any = {};

  constructor(private service: LoginService,
    private storage: StorageService,
    private global: GlobalService,
    private router: Router,
    private platform: Platform,
    private http: RestcallsService,
    private loadingController: LoadingController) { }

  ionViewWillEnter() {
    this.storage.getUser().then(storageUser => {
      if (!(storageUser === null)) {
        this.loadingController.create(
          {
            message: 'Please wait...',
          }
        ).then(loader => {
          loader.present();
          sessionStorage.setItem('accessToken', storageUser.accessToken);
          this.http.baseUrl = storageUser.url;
          this.service.getAccountDetails().subscribe((user: any) => {
            user['accessToken'] = sessionStorage.getItem('accessToken');
            this.service.saveUserData(user);
            this.global.displayToastMessage('You have successfully logged in.');
            if (user.authorities) {
              const role = user.authorities.find((element) => {
                // Checking for user having authority of initiating requests
                return element === 'ROLE_EXTERNAL_USER';
              });
              if (role) {
                loader.dismiss();
                this.router.navigate(['/tabs']);
              } else {
                loader.dismiss();
                this.router.navigate(['/tabs/inbox']);
              }
            }
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Session time out, Please login again');
          });
        });
      }
    });
  }

  getCompanyDetails() {
    this.loadingController.create(
      {
        message: 'Please Wait...',
        backdropDismiss: false,
      }
    ).then( loader => {
      loader.present();
      this.service.getCompanyDetails(this.companyCode).subscribe( (response: any) => {
        this.http.baseUrl = response.externalURL;
        this.isAccountIDVerified = true;
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to fetch company details');
      });
    });
  }

  ionViewDidLeave() {
    this.data = {};
  }

  ngOnInit() { }

  onSubmit(form) {
    this.loadingController.create(
      {
        message: 'Please wait while you are getting logged in...',
      }
    ).then(loader => {
      loader.present();
      this.service.authenticateUser(this.data)
        .subscribe((response: any) => {
          sessionStorage.setItem('accessToken', response.id_token);
          this.service.getAccountDetails().subscribe((user: any) => {
            user['accessToken'] = response.id_token;
            user['url'] = this.http.baseUrl;
            this.service.saveUserData(user);
            this.global.displayToastMessage('You have successfully logged in.');
            if (user.authorities) {
              const role = user.authorities.find((element) => {
                // Checking for user having authority of initiating requests
                return element === 'ROLE_EXTERNAL_USER';
              });
              if (role) {
                loader.dismiss();
                this.router.navigate(['/tabs']);
              } else {
                loader.dismiss();
                this.router.navigate(['/tabs/inbox']);
              }
              form.reset();
            }
          }, error => {
            loader.dismiss();
            this.global.displayToastMessage('Error Occurred While Fetching User Data. Please try after some time');
          });
        }, (error: HttpErrorResponse) => {
          loader.dismiss();
          if(error.status === 401) {
            this.global.displayToastMessage('Wrong Username and Password');
          } else {
            this.global.displayToastMessage('Error Occured While Logging In. Please try after some time');
          }
        });
    });
  }

  onKeyPress(keyCode, form) {
    if (keyCode === 13) {
      if (this.data.username && this.data.password) {
        this.onSubmit(form);
      }
    }
  }

}
