import { Injectable } from '@angular/core';
import { RestcallsService } from '../../../services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class OutboxService {

  constructor(private http: RestcallsService) { }

  resolve() {
    return this.getOutboxRequests();
  }

  getOutboxRequests() {
    const url = '/requests/outbox';
    return this.http.call_GET(url);
  }
}
