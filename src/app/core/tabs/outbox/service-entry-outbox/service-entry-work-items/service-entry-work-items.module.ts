import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceEntryWorkItemsPage } from './service-entry-work-items.page';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: ServiceEntryWorkItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceEntryWorkItemsPage]
})
export class ServiceEntryWorkItemsPageModule {}
