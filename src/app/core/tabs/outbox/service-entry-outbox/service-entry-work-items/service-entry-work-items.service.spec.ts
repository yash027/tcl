import { TestBed } from '@angular/core/testing';

import { ServiceEntryWorkItemsService } from './service-entry-work-items.service';

describe('ServiceEntryWorkItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceEntryWorkItemsService = TestBed.get(ServiceEntryWorkItemsService);
    expect(service).toBeTruthy();
  });
});
