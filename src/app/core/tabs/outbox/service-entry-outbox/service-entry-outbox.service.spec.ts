import { TestBed } from '@angular/core/testing';

import { ServiceEntryOutboxService } from './service-entry-outbox.service';

describe('ServiceEntryOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceEntryOutboxService = TestBed.get(ServiceEntryOutboxService);
    expect(service).toBeTruthy();
  });
});
