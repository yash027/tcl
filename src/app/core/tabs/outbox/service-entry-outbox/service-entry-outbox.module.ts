import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceEntryOutboxPage } from './service-entry-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceEntryOutboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceEntryOutboxPage]
})
export class ServiceEntryOutboxPageModule {}
