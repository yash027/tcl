import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';
import { ServiceEntryOutboxService } from './service-entry-outbox.service';

@Component({
  selector: 'app-service-entry-outbox',
  templateUrl: './service-entry-outbox.page.html',
  styleUrls: ['./service-entry-outbox.page.scss'],
})
export class ServiceEntryOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: ServiceEntryOutboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/outbox']);
  }

}
