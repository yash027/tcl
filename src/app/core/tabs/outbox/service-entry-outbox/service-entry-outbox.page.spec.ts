import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceEntryOutboxPage } from './service-entry-outbox.page';

describe('ServiceEntryOutboxPage', () => {
  let component: ServiceEntryOutboxPage;
  let fixture: ComponentFixture<ServiceEntryOutboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceEntryOutboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceEntryOutboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
