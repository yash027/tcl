import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoProcessOutboxPage } from './po-process-outbox.page';

describe('PoProcessOutboxPage', () => {
  let component: PoProcessOutboxPage;
  let fixture: ComponentFixture<PoProcessOutboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoProcessOutboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoProcessOutboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
