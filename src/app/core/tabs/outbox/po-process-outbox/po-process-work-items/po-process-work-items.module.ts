import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PoProcessWorkItemsPage } from './po-process-work-items.page';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: PoProcessWorkItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PoProcessWorkItemsPage]
})
export class PoProcessWorkItemsPageModule {}
