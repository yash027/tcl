import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoProcessWorkItemsPage } from './po-process-work-items.page';

describe('PoProcessWorkItemsPage', () => {
  let component: PoProcessWorkItemsPage;
  let fixture: ComponentFixture<PoProcessWorkItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoProcessWorkItemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoProcessWorkItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
