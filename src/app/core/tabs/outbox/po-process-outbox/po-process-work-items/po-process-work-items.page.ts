import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { PoProcessWorkItemsService } from './po-process-work-items.service';
import { GlobalService } from '../../../../../services/global.service';
import { PreviewDocumentPage } from '../../../inbox/inbox-request/preview-document/preview-document.page';
import { LogsAndAttachmentsPage } from '../../../inbox/inbox-request/logs-and-attachments/logs-and-attachments.page';

@Component({
  selector: 'app-po-process-work-items',
  templateUrl: './po-process-work-items.page.html',
  styleUrls: ['./po-process-work-items.page.scss'],
})
export class PoProcessWorkItemsPage implements OnInit {

  requestData: any = {
    header: {},
  };

  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: PoProcessWorkItemsService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private sanitizer: DomSanitizer,
    private modalController: ModalController) { }

  ionViewWillEnter() {
    this.route.data.subscribe(data => {
      if (data.data) {
        this.global.hideBottomTabs();
        this.requestData = data.data;
        this.requestData['component'] = 'outbox';
      }
    });
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/tabs/outbox/' + this.service.parentRequestId]);
  }

  getAttachmentUrl() {
    if (this.requestData.attachments[0].url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.requestData.attachments[0].url);
    }
  }

  openActionSheet() {
    if(this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.modalController.create(
                  {
                    component: PreviewDocumentPage,
                    componentProps: {
                      data: this.requestData.attachments[0].url
                    }
                  }
                ).then(modal => modal.present());
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

}
