import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { RestcallsService } from '../../../../../services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class PoProcessWorkItemsService {

  requestId: any;
  parentRequestId: any;

  constructor(private http: RestcallsService) { }

  resolve(route: ActivatedRouteSnapshot) {
    if (route.parent.routeConfig.path && route.params.requestId) {
      this.requestId = route.params.requestId;
      this.parentRequestId = route.parent.routeConfig.path;
      return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
    }
  }

  getRequestIdDetails(parent, requestId) {
    const url = '/requests/outbox/requestType/' + parent + '/requests/' + requestId;
    return this.http.call_GET(url);
  }
}
