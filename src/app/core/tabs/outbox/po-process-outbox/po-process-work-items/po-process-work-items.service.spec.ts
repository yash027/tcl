import { TestBed } from '@angular/core/testing';

import { PoProcessWorkItemsService } from './po-process-work-items.service';

describe('PoProcessWorkItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoProcessWorkItemsService = TestBed.get(PoProcessWorkItemsService);
    expect(service).toBeTruthy();
  });
});
