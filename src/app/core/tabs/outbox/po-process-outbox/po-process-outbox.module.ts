import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PoProcessOutboxPage } from './po-process-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: PoProcessOutboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PoProcessOutboxPage]
})
export class PoProcessOutboxPageModule {}
