import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';
import { RestcallsService } from '../../../../services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class PoProcessOutboxService {

  requestType: any;

  constructor(private global: GlobalService,
              private http: RestcallsService) { }

  resolve(route: ActivatedRouteSnapshot) {
    if(route.parent.routeConfig.path){
      this.requestType = route.parent.routeConfig.path;
      return this.getListData();
    } else {
      this.global.gotoLogin(); 
    }
  }

  getListData() {
    const url = '/requestsList/outbox/requestType/' + this.requestType;
    return this.http.call_GET(url);
  }
}