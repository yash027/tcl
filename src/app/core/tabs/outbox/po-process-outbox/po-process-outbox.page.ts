import { Component, OnInit } from '@angular/core';
import { PoProcessOutboxService } from './po-process-outbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';

@Component({
  selector: 'app-po-process-outbox',
  templateUrl: './po-process-outbox.page.html',
  styleUrls: ['./po-process-outbox.page.scss'],
})
export class PoProcessOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: PoProcessOutboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/outbox']);
  }

}
