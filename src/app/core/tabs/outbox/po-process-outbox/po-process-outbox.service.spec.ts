import { TestBed } from '@angular/core/testing';

import { PoProcessOutboxService } from './po-process-outbox.service';

describe('PoProcessOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoProcessOutboxService = TestBed.get(PoProcessOutboxService);
    expect(service).toBeTruthy();
  });
});
