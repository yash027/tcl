import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../services/global.service';

@Component({
  selector: 'app-outbox',
  templateUrl: './outbox.page.html',
  styleUrls: ['./outbox.page.scss'],
})
export class OutboxPage implements OnInit {

  requests: any = [];

  constructor(private activatedRoute: ActivatedRoute, 
              public global: GlobalService,
              private router: Router) { }

  ionViewWillEnter() {
    this.activatedRoute.data.subscribe( data => {
      if(data.data) {
        data = data.data;
        this.requests = data;
      }
    });
  }

  ngOnInit() {
  }

  onSelectRequest(requestType) {
    this.router.navigate(['/tabs/inbox/' + requestType]);
  }

}
