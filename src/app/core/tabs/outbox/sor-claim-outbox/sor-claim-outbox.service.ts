import { Injectable } from '@angular/core';
import { RestcallsService } from '../../../../services/restcalls.service';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';
@Injectable({
  providedIn: 'root'
})
export class SorClaimOutboxService {

  requestType: any;

  pageNumber: number = 0;

  pageDetails: any = {};

  constructor(private http: RestcallsService, private global: GlobalService) { }

  resolve(route: ActivatedRoute) {
    if(route.routeConfig.path){
      this.requestType = route.routeConfig.path;
      return this.getRequestWorkitems();
    } else {
      this.global.gotoLogin(); 
    }
  }

  getRequestWorkitems() {
    const url = '/requestsList/outbox/requestType/' + this.requestType + '?page=' + this.pageNumber + '&size=50&uniQueHeaderList=CONT_NAME%2CEMP_ENG_NAME%2CTG_DES%2CDEPT_DES';
    return this.http.call_GET(url);
  }
}
