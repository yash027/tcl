import { TestBed } from '@angular/core/testing';

import { SorClaimOutboxService } from './sor-claim-outbox.service';

describe('SorClaimOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SorClaimOutboxService = TestBed.get(SorClaimOutboxService);
    expect(service).toBeTruthy();
  });
});
