import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SorClaimOutboxPage } from './sor-claim-outbox.page';
import { MatExpansionModule } from '@angular/material';
import { PipesModule } from '../../../../services/pipes/pipes.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const routes: Routes = [
  {
    path: '',
    component: SorClaimOutboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    Ng2SearchPipeModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SorClaimOutboxPage]
})
export class SorClaimOutboxPageModule {}
