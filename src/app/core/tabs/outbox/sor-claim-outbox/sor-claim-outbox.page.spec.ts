import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SorClaimOutboxPage } from './sor-claim-outbox.page';

describe('SorClaimOutboxPage', () => {
  let component: SorClaimOutboxPage;
  let fixture: ComponentFixture<SorClaimOutboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SorClaimOutboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SorClaimOutboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
