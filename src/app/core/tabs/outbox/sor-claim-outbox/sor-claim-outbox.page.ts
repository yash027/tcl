import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SorClaimOutboxService } from './sor-claim-outbox.service';
import { GlobalService } from '../../../../services/global.service';
import { ModalController, LoadingController, IonContent } from '@ionic/angular';
import { AttachmentsModalPage } from '../../inbox/inbox-request/attachments-modal/attachments-modal.page';
import { LogsModalPage } from '../../inbox/inbox-request/logs-modal/logs-modal.page';

@Component({
  selector: 'app-sor-claim-outbox',
  templateUrl: './sor-claim-outbox.page.html',
  styleUrls: ['./sor-claim-outbox.page.scss'],
})
export class SorClaimOutboxPage implements OnInit {

  @ViewChild('page') pageTop: IonContent;

  public pageScroller(){
    this.pageTop.scrollToTop();
  }

  requestData: any = {
    RequestList: []
  };

  filteredSearch: any = {
    CONT_NAME: '',
    EMP_ENG_NAME: '',
    TG_DES: ''
  }

  searchText: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              public service: SorClaimOutboxService,
              public global: GlobalService,
              private modalController: ModalController,
              private loadingController: LoadingController
    ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe( data => {
      this.service.pageDetails = data.data;
      this.requestData = this.service.pageDetails.content[0];
    }, error => {
      this.global.displayToastMessage('Unable to fetch data from server, Please try after some time');
      this.global.gotoLogin();
    });
  }

  ionViewWillLeave() {
    this.global.showBottomTabs();
  }

  getRequestWorkitems() {
    this.loadingController.create(
      {
        message: 'Please wait...',
      }
    ).then( loader => {
      loader.present();
      this.service.getRequestWorkitems().subscribe( (response: any) => {
        this.service.pageDetails = response;
        this.requestData = this.service.pageDetails.content[0];
        loader.dismiss();
        this.pageScroller();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to fetch data, please try after sometime.');
        this.global.gotoLogin();
      });
    });
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/tabs/outbox']);
  }

  changePage(type) {
    if(type == 'increment') {
      this.service.pageNumber++;
    } else {
      this.service.pageNumber--;
    }
  }

  onViewAttachment(request) {
    this.modalController.create(
      {
        component: AttachmentsModalPage,
        componentProps: {
          data: {
            attachments: request.attachments,
            requestId: request.requestId
          }
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onViewLogs(request) {
    this.modalController.create(
      {
        component: LogsModalPage,
        componentProps: {
          data: request.logs
        }
      }
    ).then( modal => {
      modal.present();
    });
  }
}
