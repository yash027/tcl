import { TestBed } from '@angular/core/testing';

import { SaProcessOutboxService } from './sa-process-outbox.service';

describe('SaProcessOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaProcessOutboxService = TestBed.get(SaProcessOutboxService);
    expect(service).toBeTruthy();
  });
});
