import { Component, OnInit } from '@angular/core';
import { SaProcessOutboxService } from './sa-process-outbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';

@Component({
  selector: 'app-sa-process-outbox',
  templateUrl: './sa-process-outbox.page.html',
  styleUrls: ['./sa-process-outbox.page.scss'],
})
export class SaProcessOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: SaProcessOutboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/outbox']);
  }
}
