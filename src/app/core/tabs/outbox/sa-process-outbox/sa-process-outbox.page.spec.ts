import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaProcessOutboxPage } from './sa-process-outbox.page';

describe('SaProcessOutboxPage', () => {
  let component: SaProcessOutboxPage;
  let fixture: ComponentFixture<SaProcessOutboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaProcessOutboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaProcessOutboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
