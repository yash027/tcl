import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaProcessWorkItemsPage } from './sa-process-work-items.page';

describe('SaProcessWorkItemsPage', () => {
  let component: SaProcessWorkItemsPage;
  let fixture: ComponentFixture<SaProcessWorkItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaProcessWorkItemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaProcessWorkItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
