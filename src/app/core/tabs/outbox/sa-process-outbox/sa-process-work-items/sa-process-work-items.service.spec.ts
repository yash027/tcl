import { TestBed } from '@angular/core/testing';

import { SaProcessWorkItemsService } from './sa-process-work-items.service';

describe('SaProcessWorkItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaProcessWorkItemsService = TestBed.get(SaProcessWorkItemsService);
    expect(service).toBeTruthy();
  });
});
