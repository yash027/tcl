import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SaProcessWorkItemsPage } from './sa-process-work-items.page';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: SaProcessWorkItemsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SaProcessWorkItemsPage]
})
export class SaProcessWorkItemsPageModule {}
