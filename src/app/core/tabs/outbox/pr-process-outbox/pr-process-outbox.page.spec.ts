import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrProcessOutboxPage } from './pr-process-outbox.page';

describe('PrProcessOutboxPage', () => {
  let component: PrProcessOutboxPage;
  let fixture: ComponentFixture<PrProcessOutboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrProcessOutboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrProcessOutboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
