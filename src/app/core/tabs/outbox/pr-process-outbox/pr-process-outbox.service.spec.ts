import { TestBed } from '@angular/core/testing';

import { PrProcessOutboxService } from './pr-process-outbox.service';

describe('PrProcessOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrProcessOutboxService = TestBed.get(PrProcessOutboxService);
    expect(service).toBeTruthy();
  });
});
