import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrProcessOutboxPage } from './pr-process-outbox.page';

const routes: Routes = [
  {
    path: '',
    component: PrProcessOutboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PrProcessOutboxPage]
})
export class PrProcessOutboxPageModule {}
