import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../services/global.service';
import { PrProcessOutboxService } from './pr-process-outbox.service';

@Component({
  selector: 'app-pr-process-outbox',
  templateUrl: './pr-process-outbox.page.html',
  styleUrls: ['./pr-process-outbox.page.scss'],
})
export class PrProcessOutboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: PrProcessOutboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/outbox']);
  }

}
