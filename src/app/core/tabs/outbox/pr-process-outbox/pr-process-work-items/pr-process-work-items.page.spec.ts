import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrProcessWorkItemsPage } from './pr-process-work-items.page';

describe('PrProcessWorkItemsPage', () => {
  let component: PrProcessWorkItemsPage;
  let fixture: ComponentFixture<PrProcessWorkItemsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrProcessWorkItemsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrProcessWorkItemsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
