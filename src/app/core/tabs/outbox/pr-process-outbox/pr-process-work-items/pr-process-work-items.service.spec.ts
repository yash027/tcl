import { TestBed } from '@angular/core/testing';

import { PrProcessWorkItemsService } from './pr-process-work-items.service';

describe('PrProcessWorkItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrProcessWorkItemsService = TestBed.get(PrProcessWorkItemsService);
    expect(service).toBeTruthy();
  });
});
