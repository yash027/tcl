import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TimesheetClaimOutboxPage } from './timesheet-claim-outbox.page';
import { MatExpansionModule } from '@angular/material';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

const routes: Routes = [
  {
    path: '',
    component: TimesheetClaimOutboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TimesheetClaimOutboxPage]
})
export class TimesheetClaimOutboxPageModule {}
