import { TestBed } from '@angular/core/testing';

import { TimesheetClaimOutboxService } from './timesheet-claim-outbox.service';

describe('TimesheetClaimOutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimesheetClaimOutboxService = TestBed.get(TimesheetClaimOutboxService);
    expect(service).toBeTruthy();
  });
});
