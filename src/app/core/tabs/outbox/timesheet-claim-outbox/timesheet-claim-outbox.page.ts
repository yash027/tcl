import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TimesheetClaimOutboxService } from './timesheet-claim-outbox.service';
import { GlobalService } from '../../../../services/global.service';
import { ModalController } from '@ionic/angular';
import { AttachmentsModalPage } from '../../inbox/inbox-request/attachments-modal/attachments-modal.page';
import { LogsModalPage } from '../../inbox/inbox-request/logs-modal/logs-modal.page';

@Component({
  selector: 'app-timesheet-claim-outbox',
  templateUrl: './timesheet-claim-outbox.page.html',
  styleUrls: ['./timesheet-claim-outbox.page.scss'],
})
export class TimesheetClaimOutboxPage implements OnInit {

  requestData: any = {
    RequestList: []
  };

  filteredSearch: any = {
    CONT_NAME: '',
    EMP_ENG_NAME: '',
    TG_DES: ''
  }

  searchText: any;

  lineItemLoading: boolean;

  constructor(private route: ActivatedRoute,
              private router: Router,
              public service: TimesheetClaimOutboxService,
              public global: GlobalService,
              private modalController: ModalController
    ) { }

  ionViewWillEnter() {
    this.global.hideBottomTabs();
    this.route.data.subscribe( data => {
      this.service.pageDetails = data.data;
      this.requestData = this.service.pageDetails.content[0];
    }, error => {
      this.global.displayToastMessage('Unable to fetch data from server, Please try after some time');
      this.global.gotoLogin();
    });
  }

  ionViewWillLeave() {
    this.global.showBottomTabs();
  }

  ngOnInit() {}

  onBack() {
    this.router.navigate(['/tabs/outbox']);
  }

  changePage(type) {
    if(type == 'increment') {
      this.service.pageNumber++;
    } else {
      this.service.pageNumber--;
    }
  }

  onViewAttachment(request) {
    this.modalController.create(
      {
        component: AttachmentsModalPage,
        componentProps: {
          data: {
            attachments: request.attachments,
            requestId: request.requestId
          }
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onViewLogs(request) {
    this.modalController.create(
      {
        component: LogsModalPage,
        componentProps: {
          data: request.logs
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

}
