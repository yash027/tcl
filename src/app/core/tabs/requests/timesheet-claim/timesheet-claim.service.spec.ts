import { TestBed } from '@angular/core/testing';

import { TimesheetClaimService } from './timesheet-claim.service';

describe('TimesheetClaimService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimesheetClaimService = TestBed.get(TimesheetClaimService);
    expect(service).toBeTruthy();
  });
});
