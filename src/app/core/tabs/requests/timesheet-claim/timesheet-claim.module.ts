import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TimesheetClaimPage } from './timesheet-claim.page';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: TimesheetClaimPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatExpansionModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TimesheetClaimPage]
})
export class TimesheetClaimPageModule {}
