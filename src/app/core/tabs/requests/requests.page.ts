import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../../services/global.service';
@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {

  requests: any = [
    {
      imgText: 'SOR Claim',
      title: 'SOR Claim',
      routeUrl: 'sor-claim'
    },
    {
      imgText: 'Timesheet Claim',
      title: 'Timesheet Claim',
      routeUrl: 'timesheet-claim'
    }
  ]

  constructor(public global: GlobalService) { }

  ngOnInit() {
  }

}
