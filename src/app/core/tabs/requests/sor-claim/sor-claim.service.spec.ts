import { TestBed } from '@angular/core/testing';

import { SorClaimService } from './sor-claim.service';

describe('SorClaimService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SorClaimService = TestBed.get(SorClaimService);
    expect(service).toBeTruthy();
  });
});
