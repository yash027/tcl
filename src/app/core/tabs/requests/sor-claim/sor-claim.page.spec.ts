import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SorClaimPage } from './sor-claim.page';

describe('SorClaimPage', () => {
  let component: SorClaimPage;
  let fixture: ComponentFixture<SorClaimPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SorClaimPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SorClaimPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
