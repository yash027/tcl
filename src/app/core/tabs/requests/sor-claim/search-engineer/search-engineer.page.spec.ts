import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEngineerPage } from './search-engineer.page';

describe('SearchEngineerPage', () => {
  let component: SearchEngineerPage;
  let fixture: ComponentFixture<SearchEngineerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchEngineerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEngineerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
