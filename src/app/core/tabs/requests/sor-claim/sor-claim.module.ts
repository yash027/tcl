import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SorClaimPage } from './sor-claim.page';

import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: SorClaimPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatExpansionModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SorClaimPage]
})
export class SorClaimPageModule {}
