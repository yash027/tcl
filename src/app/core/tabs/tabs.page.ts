import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  externalUser: any;

  constructor() { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    const userRole = localStorage.getItem('role');
    if(userRole) {
      const roles = userRole.split(',');
      roles.forEach( role => {
        if(role === 'ROLE_EXTERNAL_USER') {
          this.externalUser = true;
        }
      });
    }
  }

}
