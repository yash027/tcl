import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuardService } from '../../services/auth-guards/auth-guard.service';

import { InboxService } from './inbox/inbox.service';
import { OutboxService } from './outbox/outbox.service';
import { TimesheetClaimService } from './requests/timesheet-claim/timesheet-claim.service';
import { SorInboxService } from './inbox/inbox-request/sor-inbox/sor-inbox.service';
import { SaProcessWorkItemService } from './inbox/inbox-request/sa-process-inbox/sa-process-work-item/sa-process-work-item.service';
import { TimeclaimInboxService } from './inbox/inbox-request/timeclaim-inbox/timeclaim-inbox.service';
import { PoProcessInboxService } from './inbox/inbox-request/po-process-inbox/po-process-inbox.service';
import { PoProcessWorkItemService } from './inbox/inbox-request/po-process-inbox/po-process-work-item/po-process-work-item.service';
import { PrProcessInboxService } from './inbox/inbox-request/pr-process-inbox/pr-process-inbox.service';
import { PrProcessWorkItemService } from './inbox/inbox-request/pr-process-inbox/pr-process-work-item/pr-process-work-item.service';
import { ServiceEntryWorkItemService } from './inbox/inbox-request/service-entry-inbox/service-entry-work-item/service-entry-work-item.service';
import { ServiceEntryInboxService } from './inbox/inbox-request/service-entry-inbox/service-entry-inbox.service';
import { SaProcessInboxService } from './inbox/inbox-request/sa-process-inbox/sa-process-inbox.service';

import { TimesheetClaimOutboxService } from './outbox/timesheet-claim-outbox/timesheet-claim-outbox.service';
import { SorClaimOutboxService } from './outbox/sor-claim-outbox/sor-claim-outbox.service';
import { PoProcessOutboxService } from './outbox/po-process-outbox/po-process-outbox.service';
import { PoProcessWorkItemsService } from './outbox/po-process-outbox/po-process-work-items/po-process-work-items.service';
import { PrProcessWorkItemsService } from './outbox/pr-process-outbox/pr-process-work-items/pr-process-work-items.service';
import { PrProcessOutboxService } from './outbox/pr-process-outbox/pr-process-outbox.service';
import { SaProcessWorkItemsService } from './outbox/sa-process-outbox/sa-process-work-items/sa-process-work-items.service';
import { ServiceEntryOutboxService } from './outbox/service-entry-outbox/service-entry-outbox.service';
import { ServiceEntryWorkItemsService } from './outbox/service-entry-outbox/service-entry-work-items/service-entry-work-items.service';
import { SaProcessOutboxService } from './outbox/sa-process-outbox/sa-process-outbox.service';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'requests',
        children: [
          {
            path: '',
            loadChildren: './requests/requests.module#RequestsPageModule',
            canActivate: [AuthGuardService]
          },
          {
            path: 'sor-claim',
            loadChildren: './requests/sor-claim/sor-claim.module#SorClaimPageModule',
            canActivate: [AuthGuardService]
          },
          {
            path: 'timesheet-claim',
            loadChildren: './requests/timesheet-claim/timesheet-claim.module#TimesheetClaimPageModule',
            resolve: { data: TimesheetClaimService },
            canActivate: [AuthGuardService]
          }
        ]
      },
      {
        path: 'inbox',
        children: [
          {
            path: '',
            loadChildren: './inbox/inbox.module#InboxPageModule',
            resolve: { data: InboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'SOR_CLAIM',
            loadChildren: './inbox/inbox-request/sor-inbox/sor-inbox.module#SorInboxPageModule',
            resolve: { data: SorInboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'TIME_CLAIM',
            loadChildren: './inbox/inbox-request/timeclaim-inbox/timeclaim-inbox.module#TimeclaimInboxPageModule',
            resolve: { data: TimeclaimInboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'PO_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './inbox/inbox-request/po-process-inbox/po-process-inbox.module#PoProcessInboxPageModule',
                resolve: { data: PoProcessInboxService },
                canActivate: [AuthGuardService]
              }, {
                path: ':requestId',
                loadChildren: './inbox/inbox-request/po-process-inbox/po-process-work-item/po-process-work-item.module#PoProcessWorkItemPageModule',
                resolve: { data: PoProcessWorkItemService },
                canActivate: [AuthGuardService]
              }
            ],
          },
          {
            path: 'PR_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './inbox/inbox-request/pr-process-inbox/pr-process-inbox.module#PrProcessInboxPageModule',
                resolve: { data:  PrProcessInboxService},
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './inbox/inbox-request/pr-process-inbox/pr-process-work-item/pr-process-work-item.module#PrProcessWorkItemPageModule',
                resolve: { data: PrProcessWorkItemService },
                canActivate: [AuthGuardService]
              }
            ]
          },
          {
            path: 'SERVICE_ENTRY',
            children: [
              {
                path: '',
                loadChildren: './inbox/inbox-request/service-entry-inbox/service-entry-inbox.module#ServiceEntryInboxPageModule',
                resolve: { data: ServiceEntryInboxService },
                canActivate: [AuthGuardService]
              }, {
                path: ':requestId',
                loadChildren: './inbox/inbox-request/service-entry-inbox/service-entry-work-item/service-entry-work-item.module#ServiceEntryWorkItemPageModule',
                resolve: { data: ServiceEntryWorkItemService },
                canActivate: [AuthGuardService]
              }
            ],
          },
          {
            path: 'SA_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './inbox/inbox-request/sa-process-inbox/sa-process-inbox.module#SaProcessInboxPageModule',
                resolve: { data: SaProcessInboxService },
                canActivate: [AuthGuardService]
              }, {
                path: ':requestId',
                loadChildren: './inbox/inbox-request/sa-process-inbox/sa-process-work-item/sa-process-work-item.module#SaProcessWorkItemPageModule',
                resolve: { data: SaProcessWorkItemService },
                canActivate: [AuthGuardService]
              }
            ],
          },
          {
            path: 'CONTRACTS',
            children: [
              {
                path: '',
                loadChildren: './inbox/inbox-request/sa-process-inbox/sa-process-inbox.module#SaProcessInboxPageModule',
                resolve: { data: SaProcessInboxService },
                canActivate: [AuthGuardService]
              }, {
                path: ':requestId',
                loadChildren: './inbox/inbox-request/sa-process-inbox/sa-process-work-item/sa-process-work-item.module#SaProcessWorkItemPageModule',
                resolve: { data: SaProcessWorkItemService },
                canActivate: [AuthGuardService]
              }
            ],
          }
        ]
      },
      {
        path: 'outbox',
        children: [
          {
            path: '',
            loadChildren: './outbox/outbox.module#OutboxPageModule',
            resolve: { data: OutboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'SOR_CLAIM',
            loadChildren: './outbox/sor-claim-outbox/sor-claim-outbox.module#SorClaimOutboxPageModule',
            resolve: { data: SorClaimOutboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'TIME_CLAIM',
            loadChildren: './outbox/timesheet-claim-outbox/timesheet-claim-outbox.module#TimesheetClaimOutboxPageModule',
            resolve: { data: TimesheetClaimOutboxService },
            canActivate: [AuthGuardService]
          },
          {
            path: 'PO_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './outbox/po-process-outbox/po-process-outbox.module#PoProcessOutboxPageModule',
                resolve: { data: PoProcessOutboxService },
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './outbox/po-process-outbox/po-process-work-items/po-process-work-items.module#PoProcessWorkItemsPageModule',
                resolve: { data: PoProcessWorkItemsService },
                canActivate: [AuthGuardService]
              }
            ]
          },
          {
            path: 'PR_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './outbox/pr-process-outbox/pr-process-outbox.module#PrProcessOutboxPageModule',
                resolve: { data: PrProcessOutboxService },
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './outbox/pr-process-outbox/pr-process-work-items/pr-process-work-items.module#PrProcessWorkItemsPageModule',
                resolve: { data: PrProcessWorkItemsService },
                canActivate: [AuthGuardService]
              }
            ]
          },
          {
            path: 'SA_PROCESS',
            children: [
              {
                path: '',
                loadChildren: './outbox/sa-process-outbox/sa-process-outbox.module#SaProcessOutboxPageModule',
                resolve: { data: SaProcessOutboxService },
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './outbox/sa-process-outbox/sa-process-work-items/sa-process-work-items.module#SaProcessWorkItemsPageModule',
                resolve: { data: SaProcessWorkItemsService },
                canActivate: [AuthGuardService]
              }
            ]
          },
          {
            path: 'CONTRACTS',
            children: [
              {
                path: '',
                loadChildren: './outbox/sa-process-outbox/sa-process-outbox.module#SaProcessOutboxPageModule',
                resolve: { data: SaProcessOutboxService },
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './outbox/sa-process-outbox/sa-process-work-items/sa-process-work-items.module#SaProcessWorkItemsPageModule',
                resolve: { data: SaProcessWorkItemsService },
                canActivate: [AuthGuardService]
              }
            ]
          },
          {
            path: 'SERVICE_ENTRY',
            children: [
              {
                path: '',
                loadChildren: './outbox/service-entry-outbox/service-entry-outbox.module#ServiceEntryOutboxPageModule',
                resolve: { data: ServiceEntryOutboxService },
                canActivate: [AuthGuardService]
              },
              {
                path: ':requestId',
                loadChildren: './outbox/service-entry-outbox/service-entry-work-items/service-entry-work-items.module#ServiceEntryWorkItemsPageModule',
                resolve: { data: ServiceEntryWorkItemsService },
                canActivate: [AuthGuardService]
              }
            ]
          },
        ]
      },
      {
        path: 'settings',
        children: [
          {
            path: '',
            loadChildren: './settings/settings.module#SettingsPageModule',
            canActivate: [AuthGuardService]
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/requests',
        pathMatch: 'full',
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/requests',
    pathMatch: 'full',
    canActivate: [AuthGuardService]
  }, 
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsRoutingModule {}
