import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../services/storage.service';
import { GlobalService } from '../../../services/global.service';
import { AlertController } from '@ionic/angular';
import { AlertPromise } from 'selenium-webdriver';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  userDetails: any = {};

  constructor(private storage: StorageService, private global: GlobalService, private alertController: AlertController) { }

  ionViewWillEnter() {
    this.storage.getUser().then( user => {
      if(user && user != null){
        this.userDetails = user;
      } else {
        this.global.displayToastMessage('You need to login first.');
        this.global.gotoLogin();
      }
    });
  }

  onLogout() {
    this.alertController.create({
      header: 'Message',
      message: 'Are you sure, you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler: () => {
            sessionStorage.clear();
            localStorage.clear();
            this.storage.deleteUser();
            this.global.displayToastMessage('You have successfully logged out');
            this.global.gotoLogin();
          }
        }
      ]
    }).then( alert => alert.present());
  }

  ngOnInit() {
  }

}
