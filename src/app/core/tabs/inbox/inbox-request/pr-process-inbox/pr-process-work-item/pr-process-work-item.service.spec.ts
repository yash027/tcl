import { TestBed } from '@angular/core/testing';

import { PrProcessWorkItemService } from './pr-process-work-item.service';

describe('PrProcessWorkItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrProcessWorkItemService = TestBed.get(PrProcessWorkItemService);
    expect(service).toBeTruthy();
  });
});
