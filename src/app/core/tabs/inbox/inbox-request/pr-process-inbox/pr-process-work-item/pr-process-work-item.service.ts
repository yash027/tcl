import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { RestcallsService } from '../../../../../../services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class PrProcessWorkItemService {

  requestId: any;
  parentRequestId: any;

  constructor(private http: RestcallsService) { }

  resolve(route: ActivatedRouteSnapshot) {
    if(route.parent.routeConfig.path && route.params.requestId) {
      this.requestId = route.params.requestId;
      this.parentRequestId = route.parent.routeConfig.path;
      return this.getRequestIdDetails(route.parent.routeConfig.path, this.requestId);
    }
  }

  getRequestIdDetails(parent, requestId) {
    const url = '/requests/inbox/requestType/' + parent + '/requests/' + requestId;
    return this.http.call_GET(url);
  }

  submitWorkItem(data, attachments, images) {
    const url = '/requests/inbox/requestType/' + this.parentRequestId + '/requests/' + this.requestId + '/submit';
    const formData = new FormData();
    for (let key in attachments) {
      formData.append('files', attachments[key]);
    }
    images.forEach(element => {
      formData.append('files', element.image, element.name);
    });
    formData.append('requestData', JSON.stringify(data));
    return this.http.call_POST(url, formData);
  }
}
