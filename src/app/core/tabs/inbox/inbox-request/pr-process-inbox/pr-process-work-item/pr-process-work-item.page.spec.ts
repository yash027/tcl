import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrProcessWorkItemPage } from './pr-process-work-item.page';

describe('PrProcessWorkItemPage', () => {
  let component: PrProcessWorkItemPage;
  let fixture: ComponentFixture<PrProcessWorkItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrProcessWorkItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrProcessWorkItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
