import { TestBed } from '@angular/core/testing';

import { PrProcessInboxService } from './pr-process-inbox.service';

describe('PrProcessInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PrProcessInboxService = TestBed.get(PrProcessInboxService);
    expect(service).toBeTruthy();
  });
});
