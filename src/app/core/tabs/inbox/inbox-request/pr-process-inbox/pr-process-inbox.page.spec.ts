import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrProcessInboxPage } from './pr-process-inbox.page';

describe('PrProcessInboxPage', () => {
  let component: PrProcessInboxPage;
  let fixture: ComponentFixture<PrProcessInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrProcessInboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrProcessInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
