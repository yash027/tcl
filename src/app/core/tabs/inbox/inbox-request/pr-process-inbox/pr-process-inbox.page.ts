import { Component, OnInit } from '@angular/core';
import { PrProcessInboxService } from './pr-process-inbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../services/global.service';

@Component({
  selector: 'app-pr-process-inbox',
  templateUrl: './pr-process-inbox.page.html',
  styleUrls: ['./pr-process-inbox.page.scss'],
})
export class PrProcessInboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: PrProcessInboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/inbox']);
  }

}
