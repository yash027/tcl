import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PrProcessInboxPage } from './pr-process-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: PrProcessInboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PrProcessInboxPage]
})
export class PrProcessInboxPageModule {}
