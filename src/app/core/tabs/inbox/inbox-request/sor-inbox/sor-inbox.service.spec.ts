import { TestBed } from '@angular/core/testing';

import { SorInboxService } from './sor-inbox.service';

describe('SorInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SorInboxService = TestBed.get(SorInboxService);
    expect(service).toBeTruthy();
  });
});
