import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SorInboxPage } from './sor-inbox.page';

describe('SorInboxPage', () => {
  let component: SorInboxPage;
  let fixture: ComponentFixture<SorInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SorInboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SorInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
