import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SorInboxService } from './sor-inbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../services/global.service';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { AttachmentsModalPage } from './../attachments-modal/attachments-modal.page';
import { LogsModalPage } from './../logs-modal/logs-modal.page';
import { SubmissionModalPage } from './../submission-modal/submission-modal.page';
import { ChangeDocumentHistoryPage } from '../change-document-history/change-document-history.page';

@Component({
  selector: 'app-sor-inbox',
  templateUrl: './sor-inbox.page.html',
  styleUrls: ['./sor-inbox.page.scss'],
})
export class SorInboxPage implements OnInit {

  requestData: any = {
    RequestList: []
  };

  filteredSearch: any = {
    CONT_NAME: '',
    EMP_ENG_NAME: '',
    TG_DES: ''
  }

  searchText: any;

  selectedWorkItems: any = [];

  approvalTypes: any = ['All', 'Approved', 'Rejected'];

  currentApproval: any;

  constructor(private activatedRoute: ActivatedRoute,
    public service: SorInboxService,
    private router: Router,
    public global: GlobalService,
    private modalController: ModalController,
    private loadingController: LoadingController,
    private alertController: AlertController) { }

  ionViewWillEnter() {
    this.selectedWorkItems.length = 0;
    this.requestData = {
      RequestList: []
    };
    this.global.hideBottomTabs();
    this.activatedRoute.data.subscribe(data => {
      if (data.data) {
        data.data.content[0].RequestList.forEach(element => {
          element['CAN_EDIT'] = false;
        });
        this.requestData = data.data.content[0];
      } else {
        this.global.displayToastMessage('Some problem occured while fetching data, Please try after some time');
        this.global.gotoLogin();
      }
    });
  }

  ionViewWillLeave() {
    this.global.showBottomTabs();
  }

  ngOnInit() {
  }

  onFilterSearch() {
  }

  onBack() {
    this.router.navigate(['/tabs/inbox']);
  }

  onApprovalTypeChange(value) {
    this.currentApproval = value;
    this.loadingController.create(
      {
        message: 'Please Wait...',
      }
    ).then( loader => {
      loader.present();
      this.service.getRequestTypeData(this.service.requestType, this.service.setApprovalType(value)).subscribe( (response: any) => {
        response.content[0].RequestList.forEach(element => {
          element['CAN_EDIT'] = false;
        });
        this.requestData = response.content[0];
        this.selectedWorkItems.length = 0;
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Some problem occured while fetching data, Please try after some time');
        this.global.gotoLogin();
      });
    });
  }

  onEditWorkItem(request) {
    this.requestData.RequestList.forEach(element => {
      if(element.id === request.id) {
        element.CAN_EDIT = true;
      } else {
        element.CAN_EDIT = false;
      }
    });
    this.global.displayToastMessage('Now you can edit the Quantity Field');
  }

  onSelectWorkItems(request) {
    if(this.selectedWorkItems.includes(request.id)){
      this.selectedWorkItems.splice(this.selectedWorkItems.indexOf(request.id), 1);
    } else {
      this.selectedWorkItems.push(request.id);
    }
  }

  onViewAttachment(request) {
    this.modalController.create(
      {
        component: AttachmentsModalPage,
        componentProps: {
          data: {
            attachments: request.attachments,
            requestId: request.requestId
          }
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onViewLogs(request) {
    this.modalController.create(
      {
        component: LogsModalPage,
        componentProps: {
          data: request.logs
        }
      }
    ).then( modal => {
      modal.present();
    });
  }

  onQuantityCheck(request){
   if(request.header.QUANTITY === '' || request.header.QUANTITY === null){
     request.header.QUANTITY = 0;
   } 
   request.header.QUANTITY = parseInt(request.header.QUANTITY);
   request.header.AMOUNT = request.header.QUANTITY * Number(request.header.PRICE);
  }

  onSingleSubmission(request, submissionType) {
    this.modalController.create(
      {
        component: SubmissionModalPage,
        componentProps: { data: request.header.AMOUNT, requestType: 'Single ' + submissionType, currentStep: request.currentstep}
      }
    ).then( modal => {
      modal.present();
      modal.onDidDismiss().then( (data: any) => {
        if(data.data){
          data = data.data;
          this.loadingController.create(
            {
              message: 'Please wait while its working for ' + submissionType,
            }
          ).then( loader => {
            loader.present();
            let submitData: any = {
              requestDetailsList: {
              }
            };
            request.activities.forEach(element => {
              if(element.description === submissionType) {
                submitData.requestDetailsList['activities'] = [element];
              }
            });
            submitData.requestDetailsList['comment'] = data.description;
            submitData.requestDetailsList['header'] = request.header;
            if(data.INV_REF && data.INV_REF_DATE) {
              submitData.requestDetailsList.header['INV_REF'] = data.INV_REF;
              submitData.requestDetailsList.header['INV_REF_DATE'] = this.global.getSAPDate(data.INV_REF_DATE);
            }
            submitData.requestDetailsList['lineItems'] = [];
            submitData.requestDetailsList['requestId'] = request.requestId;
            submitData.requestDetailsList = [submitData.requestDetailsList];
            this.service.onSubmitWorkItems(submitData, data.attachments, data.images).subscribe( response => {
              loader.dismiss();
              this.alertController.create(
                {
                  header: 'Message',
                  message: 'You have successfully initiated for ' + submissionType,
                  backdropDismiss: false,
                  buttons: [
                    {
                      text: 'Ok',
                      handler: () => {
                        this.onApprovalTypeChange(this.currentApproval);
                      }
                    }
                  ]
                }
              ).then( alert => {
                alert.present();
              })
            }, error => {
              loader.dismiss();
              this.global.displayToastMessage('Some problem occured while initiating request.');
              this.router.navigate(['/tabs/inbox']);
            });
          });
        }
      })
    });
  }

  onViewChangeDocument(request) {
    this.loadingController.create(
      {
        message: 'Please wait...'
      }
    ).then( loader => {
      loader.present();
      this.service.getDocumentChangeHistory(request.requestId).subscribe( response => {
        loader.dismiss();
        this.modalController.create(
          {
            component: ChangeDocumentHistoryPage,
            componentProps: {
             data: response
            }
          }
        ).then( modal => {
          modal.present();
        });
      }, error => {
        loader.dismiss();
        this.global.displayToastMessage('Unable to display Document Change History, Try After Some Time');
      });
    })
  }

  getSelectedWorkItems() {
    let workitems: any = [];
    this.selectedWorkItems.forEach(element => {
      this.requestData.RequestList.forEach(request => {
        if(element === request.id) {
          workitems.push(request);
          return;
        }
      });
    });
    return workitems;
  }

  onMultipleSubmission(submissionType){
    let selectedWorkitems = this.getSelectedWorkItems();
    let currentStep = selectedWorkitems[0].currentstep;
    let workItems: any = [];
    let uniqueTG: any;
    let toStopSelection: any = true;
    let totalAmount = 0;
    selectedWorkitems.forEach( (element, index) => {
      if(toStopSelection) {
        if(!uniqueTG) {
          uniqueTG = element.header.TG_CODE;
          workItems.push(element);
          totalAmount += Number(element.header.AMOUNT);
        } else {
          if (uniqueTG !== element.header.TG_CODE) {
            toStopSelection = false;
          } else {
            workItems.push(element);
            totalAmount += Number(element.header.AMOUNT);
          }
        }
      }
    });
    if (toStopSelection) {
      this.modalController.create(
        {
          component: SubmissionModalPage,
          componentProps: { data: totalAmount, requestType: 'Bulk ' + submissionType, currentStep: currentStep}
        }
      ).then( modal => {
        modal.present();
        modal.onDidDismiss().then( (data: any) => {
          if( data.data ){
            data = data.data;
            this.loadingController.create(
              {
                message: 'Please wait while its working for ' + submissionType,
              }
            ).then( loader => {
              loader.present();
              let submitData = {
                requestDetailsList: []
              };
              workItems.forEach(element => {
                let inboxWorkItem: any = {};
                element.activities.forEach(activity => {
                  if(activity.description === submissionType) {
                    inboxWorkItem['activities'] = [activity];
                  }
                });
                inboxWorkItem['comment'] = data.description;
                inboxWorkItem['header'] = element.header;
                if(data.INV_REF && data.INV_REF_DATE) {
                  inboxWorkItem.header['INV_REF'] = data.INV_REF;
                  inboxWorkItem.header['INV_REF_DATE'] = this.global.getSAPDate(data.INV_REF_DATE);
                }
                inboxWorkItem['lineItems'] = [];
                inboxWorkItem['requestId'] = element.requestId;
                submitData.requestDetailsList.push(inboxWorkItem);
              });
              this.service.onSubmitWorkItems(submitData, data.attachments, data.images).subscribe( response => {
                 loader.dismiss();
                 this.alertController.create(
                   {
                     header: 'Message',
                     message: 'You have successfully initiated for ' + submissionType,
                     backdropDismiss: false,
                     buttons: [
                       {
                         text: 'Ok',
                         handler: () => {
                           this.router.navigate(['/tabs/inbox']);
                         }
                       }
                     ]
                   }
                 ).then( alert => {
                  alert.present();
                 })
              }, error => {
                 loader.dismiss();
                 this.global.displayToastMessage('Some problem occured while initiating request.');
                 this.router.navigate(['/tabs/inbox']);
               });
           });
          }
        });
      });
    } else {
      this.alertController.create(
        {
          header: 'Message',
          message: 'Please select workitems having same TaskGroup',
          buttons: [
            {
              text: 'Ok',
              role: 'ok'
            }
          ]
        }
      ).then(
        alert => {
          alert.present();
        }
      )
    }
  }

}
