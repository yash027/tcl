import { Component, OnInit } from '@angular/core';
import { PoProcessInboxService } from './po-process-inbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../services/global.service';

@Component({
  selector: 'app-po-process-inbox',
  templateUrl: './po-process-inbox.page.html',
  styleUrls: ['./po-process-inbox.page.scss'],
})
export class PoProcessInboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: PoProcessInboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/inbox']);
  }

}
