import { TestBed } from '@angular/core/testing';

import { PoProcessInboxService } from './po-process-inbox.service';

describe('PoProcessInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoProcessInboxService = TestBed.get(PoProcessInboxService);
    expect(service).toBeTruthy();
  });
});
