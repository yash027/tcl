import { TestBed } from '@angular/core/testing';

import { PoProcessWorkItemService } from './po-process-work-item.service';

describe('PoProcessWorkItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PoProcessWorkItemService = TestBed.get(PoProcessWorkItemService);
    expect(service).toBeTruthy();
  });
});
