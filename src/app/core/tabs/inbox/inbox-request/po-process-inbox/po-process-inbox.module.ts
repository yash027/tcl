import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PoProcessInboxPage } from './po-process-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: PoProcessInboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PoProcessInboxPage]
})
export class PoProcessInboxPageModule {}
