import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoProcessInboxPage } from './po-process-inbox.page';

describe('PoProcessInboxPage', () => {
  let component: PoProcessInboxPage;
  let fixture: ComponentFixture<PoProcessInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoProcessInboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoProcessInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
