import { Component, OnInit } from '@angular/core';
import { SaProcessInboxService } from './sa-process-inbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../services/global.service';

@Component({
  selector: 'app-sa-process-inbox',
  templateUrl: './sa-process-inbox.page.html',
  styleUrls: ['./sa-process-inbox.page.scss'],
})
export class SaProcessInboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: SaProcessInboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/inbox']);
  }
}
