import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaProcessInboxPage } from './sa-process-inbox.page';

describe('SaProcessInboxPage', () => {
  let component: SaProcessInboxPage;
  let fixture: ComponentFixture<SaProcessInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaProcessInboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaProcessInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
