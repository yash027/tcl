import { TestBed } from '@angular/core/testing';

import { SaProcessWorkItemService } from './sa-process-work-item.service';

describe('SaProcessWorkItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaProcessWorkItemService = TestBed.get(SaProcessWorkItemService);
    expect(service).toBeTruthy();
  });
});
