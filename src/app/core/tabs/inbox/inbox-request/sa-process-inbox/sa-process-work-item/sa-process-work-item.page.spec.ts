import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaProcessWorkItemPage } from './sa-process-work-item.page';

describe('SaProcessWorkItemPage', () => {
  let component: SaProcessWorkItemPage;
  let fixture: ComponentFixture<SaProcessWorkItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaProcessWorkItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaProcessWorkItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
