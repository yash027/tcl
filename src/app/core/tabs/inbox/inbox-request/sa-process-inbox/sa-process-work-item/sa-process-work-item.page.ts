import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../../services/global.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, ModalController, AlertController, LoadingController } from '@ionic/angular';
import { PreviewDocumentPage } from '../../preview-document/preview-document.page';
import { LogsAndAttachmentsPage } from '../../logs-and-attachments/logs-and-attachments.page';
import { SaProcessWorkItemService } from './sa-process-work-item.service';

@Component({
  selector: 'app-sa-process-work-item',
  templateUrl: './sa-process-work-item.page.html',
  styleUrls: ['./sa-process-work-item.page.scss'],
})
export class SaProcessWorkItemPage implements OnInit {

  attachments: any = [[],[]];

  requestData: any = {
    header: {},
  };

  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: SaProcessWorkItemService,
    public global: GlobalService,
    private actionsheet: ActionSheetController,
    private sanitizer: DomSanitizer,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private modalController: ModalController) { }

  ionViewWillEnter() {
    this.route.data.subscribe(data => {
      if (data.data) {
        this.global.hideBottomTabs();
        this.requestData = data.data;
        this.requestData['component'] = 'inbox';
      }
    });
  }

  ngOnInit() {
  }

  onBack() {
    this.router.navigate(['/tabs/inbox/' + this.service.parentRequestId]);
  }

  getAttachmentUrl() {
    if (this.requestData.attachments[0].url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.requestData.attachments[0].url);
    }
  }

  openActionSheet() {
    if(this.requestData.attachments.length > 0 && this.requestData.attachments[0].url !== null) {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Preview',
              handler: () => {
                this.modalController.create(
                  {
                    component: PreviewDocumentPage,
                    componentProps: {
                      data: this.requestData.attachments[0].url
                    }
                  }
                ).then(modal => modal.present());
              }
            },
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    } else {
      this.actionsheet.create(
        {
          header: 'Options',
          buttons: [
            {
              text: 'Logs',
              handler: () => {
                this.modalController.create(
                  {
                    component: LogsAndAttachmentsPage,
                    componentProps: {
                      data: this.requestData
                    }
                  }
                ).then(modal => {
                  modal.present();
                });
              }
            }
          ]
        }
      ).then(actionSheet => actionSheet.present());
    }

  }

  submit(activity) {
    this.alertController.create(
      {
        header: activity.description,
        backdropDismiss: false,
        inputs: [
          {
            type: 'text',
            name: 'comments',
            placeholder: 'Add Comments*'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Submit',
            handler: (data) => {
              if(data.comments) {
                const submitData = {
                  activities: [activity],
                  header: this.requestData.header,
                  comment: data.comments,
                  lineItems: this.requestData.lineItems,
                }
                this.loadingController.create(
                  {
                    message: 'Please wait while its getting initiated for ' + activity.description
                  }
                ).then( loader => {
                  loader.present();
                  this.service.submitWorkItem(submitData, this.attachments[0], this.attachments[1]).subscribe( response => {
                    this.alertController.create(
                      {
                        header: 'Message',
                        backdropDismiss: false,
                        message: 'You have successfully initiated for ' + activity.description,
                        buttons: [
                          {
                            text: 'Ok',
                            handler: () => {
                              loader.dismiss();
                              this.router.navigate(['/tabs/inbox']);
                            }
                          }
                        ]
                      }
                    ).then( successAlert =>  {
                      loader.dismiss();
                      successAlert.present();
                    });
                  }, error => {
                    loader.dismiss();
                    this.global.displayToastMessage('Some problem occured while initiating for ' + activity.description);
                    this.router.navigate(['/tabs/inbox']);
                  });
                });
              } else {
                this.global.displayToastMessage('You must have to add comments.');
              }
            }
          }
        ]
      }
    ).then( alert => alert.present());
  }

}
