import { TestBed } from '@angular/core/testing';

import { SaProcessInboxService } from './sa-process-inbox.service';

describe('SaProcessInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaProcessInboxService = TestBed.get(SaProcessInboxService);
    expect(service).toBeTruthy();
  });
});
