import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SaProcessInboxPage } from './sa-process-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: SaProcessInboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SaProcessInboxPage]
})
export class SaProcessInboxPageModule {}
