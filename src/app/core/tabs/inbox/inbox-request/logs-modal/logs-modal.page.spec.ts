import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsModalPage } from './logs-modal.page';

describe('LogsModalPage', () => {
  let component: LogsModalPage;
  let fixture: ComponentFixture<LogsModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
