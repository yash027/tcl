import { TestBed } from '@angular/core/testing';

import { ServiceEntryInboxService } from './service-entry-inbox.service';

describe('ServiceEntryInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceEntryInboxService = TestBed.get(ServiceEntryInboxService);
    expect(service).toBeTruthy();
  });
});
