import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceEntryInboxPage } from './service-entry-inbox.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceEntryInboxPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceEntryInboxPage]
})
export class ServiceEntryInboxPageModule {}
