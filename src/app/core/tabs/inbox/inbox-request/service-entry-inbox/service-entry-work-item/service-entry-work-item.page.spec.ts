import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceEntryWorkItemPage } from './service-entry-work-item.page';

describe('ServiceEntryWorkItemPage', () => {
  let component: ServiceEntryWorkItemPage;
  let fixture: ComponentFixture<ServiceEntryWorkItemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceEntryWorkItemPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceEntryWorkItemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
