import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceEntryWorkItemPage } from './service-entry-work-item.page';
import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: ServiceEntryWorkItemPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatExpansionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceEntryWorkItemPage]
})
export class ServiceEntryWorkItemPageModule {}
