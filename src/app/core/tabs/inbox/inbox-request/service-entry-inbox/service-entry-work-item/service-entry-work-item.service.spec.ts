import { TestBed } from '@angular/core/testing';

import { ServiceEntryWorkItemService } from './service-entry-work-item.service';

describe('ServiceEntryWorkItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceEntryWorkItemService = TestBed.get(ServiceEntryWorkItemService);
    expect(service).toBeTruthy();
  });
});
