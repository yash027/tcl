import { Component, OnInit } from '@angular/core';
import { ServiceEntryInboxService } from './service-entry-inbox.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from '../../../../../services/global.service';

@Component({
  selector: 'app-service-entry-inbox',
  templateUrl: './service-entry-inbox.page.html',
  styleUrls: ['./service-entry-inbox.page.scss'],
})
export class ServiceEntryInboxPage implements OnInit {

  requestData: any = {};

  constructor(public service: ServiceEntryInboxService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private global: GlobalService
              ) { }

    ionViewWillEnter() {
      this.global.hideBottomTabs();
      this.activatedRoute.data.subscribe( data => {
        this.requestData = data.data.content[0];
      });
    }

  ngOnInit() {
  }

  onBack() {
    this.global.showBottomTabs();
    this.router.navigate(['/tabs/inbox']);
  }
}
