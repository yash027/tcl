import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeclaimInboxPage } from './timeclaim-inbox.page';

describe('TimeclaimInboxPage', () => {
  let component: TimeclaimInboxPage;
  let fixture: ComponentFixture<TimeclaimInboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeclaimInboxPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeclaimInboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
