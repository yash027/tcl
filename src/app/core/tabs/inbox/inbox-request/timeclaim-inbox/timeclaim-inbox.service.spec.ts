import { TestBed } from '@angular/core/testing';

import { TimeclaimInboxService } from './timeclaim-inbox.service';

describe('TimeclaimInboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimeclaimInboxService = TestBed.get(TimeclaimInboxService);
    expect(service).toBeTruthy();
  });
});
