import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { GlobalService } from '../../../../../services/global.service';
import { PreviewDocumentPage } from '../preview-document/preview-document.page';
import { Capacitor, Plugins, CameraSource, CameraResultType } from '@capacitor/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-logs-and-attachments',
  templateUrl: './logs-and-attachments.page.html',
  styleUrls: ['./logs-and-attachments.page.scss'],
})
export class LogsAndAttachmentsPage implements OnInit {

  requestData: any = {};
  segmentValue: any = 'logs';

  attachments: any = [];
  images: any = [];

  constructor(public modalController: ModalController,
              private navParams: NavParams,
              private activatedRoute: ActivatedRoute,
              public global: GlobalService) { }

  ionViewWillEnter() {
    this.requestData =  this.navParams.get('data');
  }

  onAttachmentOpen(attachment) {
    if(attachment.url) {
      this.modalController.create(
        {
          component: PreviewDocumentPage,
          componentProps: {
            data: attachment.url
          }
        }
      ).then( modal => modal.present());   
    } else {
      this.global.displayToastMessage('URL Not Available');
    }
  }

  chooseFiles() {
    document.getElementById('fileUpload').click();
  }

  onSelectFile(event) {
    const formData = event.target.files;
    for (let key in formData) {
      if (key !== 'length' && key !== 'item') {
        this.attachments.push(formData[key]);
      }
    }
  }

  onRemoveAttachment(index) {
    this.attachments.splice(index, 1);
  }

  onTakeImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.global.displayToastMessage(
        'Unable To Open Camera');
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 60,
      source: CameraSource.Camera,
      height: 400,
      width: 300,
      correctOrientation: true,
      resultType: CameraResultType.DataUrl
    })
      .then(image => {
        const blobImg = this.global.dataURItoBlob(image.dataUrl);
        const img = {
          name: 'Image-' + new Date().toISOString(),
          image: blobImg
        }
        this.images.push(img);
      })
      .catch(error => {
        return false;
      });
  }

  onRemoveImage(index) {
    this.images.splice(index, 1);
  }

  getBytesConversion(bytes) {
    const size = this.global.getBytesConversion(bytes, 2);
    return size;
  }
  
  ngOnInit() {
  }

}
