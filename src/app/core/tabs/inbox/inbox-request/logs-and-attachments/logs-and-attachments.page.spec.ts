import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsAndAttachmentsPage } from './logs-and-attachments.page';

describe('LogsAndAttachmentsPage', () => {
  let component: LogsAndAttachmentsPage;
  let fixture: ComponentFixture<LogsAndAttachmentsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsAndAttachmentsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsAndAttachmentsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
