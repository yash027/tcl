import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewDocumentPage } from './preview-document.page';

describe('PreviewDocumentPage', () => {
  let component: PreviewDocumentPage;
  let fixture: ComponentFixture<PreviewDocumentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewDocumentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewDocumentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
