import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-preview-document',
  templateUrl: './preview-document.page.html',
  styleUrls: ['./preview-document.page.scss'],
})
export class PreviewDocumentPage implements OnInit {

  url: any;

  constructor(
    private navParams: NavParams,
    public modalController: ModalController,
    private sanitizer: DomSanitizer
  ) { }

  ionViewWillEnter() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.navParams.get('data'));
  }

  ngOnInit() {
  }

}
