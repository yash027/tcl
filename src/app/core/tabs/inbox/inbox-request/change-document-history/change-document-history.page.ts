import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-change-document-history',
  templateUrl: './change-document-history.page.html',
  styleUrls: ['./change-document-history.page.scss'],
})
export class ChangeDocumentHistoryPage implements OnInit {

  filedDetails: any = {
    property: [],
    values: []
  };

  displayedColumns: any = ['order', 'to', 'from', 'by'];

  constructor(private navParams: NavParams, public modalController: ModalController) { }

  ionViewWillEnter() {
    let data = this.navParams.get('data');
    data = data.filedDetails;
    for (let key in data) {
      if (data[key]) {
        this.filedDetails.property.push(key);
        this.filedDetails.values.push(data[key]);
      }
    }
  }

  ngOnInit() {
  }

}
