import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ChangeDocumentHistoryPage } from './change-document-history.page';
import { MatTableModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: ChangeDocumentHistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChangeDocumentHistoryPage]
})
export class ChangeDocumentHistoryPageModule {}
