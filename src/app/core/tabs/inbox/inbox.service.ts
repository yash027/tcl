import { Injectable } from '@angular/core';
import { RestcallsService } from '../../../services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor(private http: RestcallsService) { }

  resolve() {
    return this.getInboxRequests();
  }

  getInboxRequests() {
    const url = '/requests/inbox';
    return this.http.call_GET(url);
  }
}
