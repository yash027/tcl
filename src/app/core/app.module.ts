
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { IonicStorageModule } from '@ionic/storage';
import { JwtInterceptor } from './../services/jwt.interceptor';
import { AuthGuardService } from '../services/auth-guards/auth-guard.service';
import { SearchEngineerPageModule } from './tabs/requests/sor-claim/search-engineer/search-engineer.module';
import { SearchTasklistPageModule } from './tabs/requests/sor-claim/search-tasklist/search-tasklist.module';
import { AttachmentsModalPageModule } from './tabs/inbox/inbox-request/attachments-modal/attachments-modal.module';
import { LogsModalPageModule } from './tabs/inbox/inbox-request/logs-modal/logs-modal.module';
import { ImagePageModule } from './tabs/inbox/inbox-request/attachments-modal/image/image.module';
import { SubmissionModalPageModule } from './tabs/inbox/inbox-request/submission-modal/submission-modal.module';
import { ChangeDocumentHistoryPageModule } from './tabs/inbox/inbox-request/change-document-history/change-document-history.module';
import { PreviewDocumentPageModule } from './tabs/inbox/inbox-request/preview-document/preview-document.module';
import { LogsAndAttachmentsPageModule } from './tabs/inbox/inbox-request/logs-and-attachments/logs-and-attachments.module';
import { Network } from '@ionic-native/network/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserAnimationsModule, 
    HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    SearchEngineerPageModule,
    SearchTasklistPageModule,
    AttachmentsModalPageModule,
    LogsModalPageModule,
    ImagePageModule,
    SubmissionModalPageModule,
    ChangeDocumentHistoryPageModule,
    PreviewDocumentPageModule,
    LogsAndAttachmentsPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    AuthGuardService,
    Network,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
