'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">TCL documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-1afade20f2d9e7cd39c9bc02a2ee4abc"' : 'data-target="#xs-components-links-module-AppModule-1afade20f2d9e7cd39c9bc02a2ee4abc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-1afade20f2d9e7cd39c9bc02a2ee4abc"' :
                                            'id="xs-components-links-module-AppModule-1afade20f2d9e7cd39c9bc02a2ee4abc"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AttachmentsModalPageModule.html" data-type="entity-link">AttachmentsModalPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AttachmentsModalPageModule-3daac6064a0997519f4dd3f6c6e21ab6"' : 'data-target="#xs-components-links-module-AttachmentsModalPageModule-3daac6064a0997519f4dd3f6c6e21ab6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AttachmentsModalPageModule-3daac6064a0997519f4dd3f6c6e21ab6"' :
                                            'id="xs-components-links-module-AttachmentsModalPageModule-3daac6064a0997519f4dd3f6c6e21ab6"' }>
                                            <li class="link">
                                                <a href="components/AttachmentsModalPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AttachmentsModalPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChangeDocumentHistoryPageModule.html" data-type="entity-link">ChangeDocumentHistoryPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ChangeDocumentHistoryPageModule-66a19ebaeef076315e529988c442183c"' : 'data-target="#xs-components-links-module-ChangeDocumentHistoryPageModule-66a19ebaeef076315e529988c442183c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ChangeDocumentHistoryPageModule-66a19ebaeef076315e529988c442183c"' :
                                            'id="xs-components-links-module-ChangeDocumentHistoryPageModule-66a19ebaeef076315e529988c442183c"' }>
                                            <li class="link">
                                                <a href="components/ChangeDocumentHistoryPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChangeDocumentHistoryPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ImagePageModule.html" data-type="entity-link">ImagePageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ImagePageModule-f47b709d8c70761021fd5555d2f92d14"' : 'data-target="#xs-components-links-module-ImagePageModule-f47b709d8c70761021fd5555d2f92d14"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ImagePageModule-f47b709d8c70761021fd5555d2f92d14"' :
                                            'id="xs-components-links-module-ImagePageModule-f47b709d8c70761021fd5555d2f92d14"' }>
                                            <li class="link">
                                                <a href="components/ImagePage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ImagePage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/InboxPageModule.html" data-type="entity-link">InboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-InboxPageModule-ba1b74a7d027b57327cd7475192ab9d0"' : 'data-target="#xs-components-links-module-InboxPageModule-ba1b74a7d027b57327cd7475192ab9d0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-InboxPageModule-ba1b74a7d027b57327cd7475192ab9d0"' :
                                            'id="xs-components-links-module-InboxPageModule-ba1b74a7d027b57327cd7475192ab9d0"' }>
                                            <li class="link">
                                                <a href="components/InboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginPageModule.html" data-type="entity-link">LoginPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' : 'data-target="#xs-components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' :
                                            'id="xs-components-links-module-LoginPageModule-4144196129c7d8f182fc4679e3c9d036"' }>
                                            <li class="link">
                                                <a href="components/LoginPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LogsAndAttachmentsPageModule.html" data-type="entity-link">LogsAndAttachmentsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LogsAndAttachmentsPageModule-00c49917c10b6c59845231bde8575d13"' : 'data-target="#xs-components-links-module-LogsAndAttachmentsPageModule-00c49917c10b6c59845231bde8575d13"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LogsAndAttachmentsPageModule-00c49917c10b6c59845231bde8575d13"' :
                                            'id="xs-components-links-module-LogsAndAttachmentsPageModule-00c49917c10b6c59845231bde8575d13"' }>
                                            <li class="link">
                                                <a href="components/LogsAndAttachmentsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogsAndAttachmentsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LogsModalPageModule.html" data-type="entity-link">LogsModalPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LogsModalPageModule-176ab5d1860af7263f942ec8d37804a4"' : 'data-target="#xs-components-links-module-LogsModalPageModule-176ab5d1860af7263f942ec8d37804a4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LogsModalPageModule-176ab5d1860af7263f942ec8d37804a4"' :
                                            'id="xs-components-links-module-LogsModalPageModule-176ab5d1860af7263f942ec8d37804a4"' }>
                                            <li class="link">
                                                <a href="components/LogsModalPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogsModalPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OutboxPageModule.html" data-type="entity-link">OutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-OutboxPageModule-a2667e7ca9cdf93da8a85e1a93122adf"' : 'data-target="#xs-components-links-module-OutboxPageModule-a2667e7ca9cdf93da8a85e1a93122adf"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-OutboxPageModule-a2667e7ca9cdf93da8a85e1a93122adf"' :
                                            'id="xs-components-links-module-OutboxPageModule-a2667e7ca9cdf93da8a85e1a93122adf"' }>
                                            <li class="link">
                                                <a href="components/OutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PipesModule.html" data-type="entity-link">PipesModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-PipesModule-18dc9479a2d3167d6e7bcfc472f6f6f5"' : 'data-target="#xs-pipes-links-module-PipesModule-18dc9479a2d3167d6e7bcfc472f6f6f5"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-PipesModule-18dc9479a2d3167d6e7bcfc472f6f6f5"' :
                                            'id="xs-pipes-links-module-PipesModule-18dc9479a2d3167d6e7bcfc472f6f6f5"' }>
                                            <li class="link">
                                                <a href="pipes/FilterPipePipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FilterPipePipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PoProcessInboxPageModule.html" data-type="entity-link">PoProcessInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PoProcessInboxPageModule-146ee9a87dc4ddcfe0fa7ce86856cdc0"' : 'data-target="#xs-components-links-module-PoProcessInboxPageModule-146ee9a87dc4ddcfe0fa7ce86856cdc0"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PoProcessInboxPageModule-146ee9a87dc4ddcfe0fa7ce86856cdc0"' :
                                            'id="xs-components-links-module-PoProcessInboxPageModule-146ee9a87dc4ddcfe0fa7ce86856cdc0"' }>
                                            <li class="link">
                                                <a href="components/PoProcessInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PoProcessInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PoProcessOutboxPageModule.html" data-type="entity-link">PoProcessOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PoProcessOutboxPageModule-12c39060409087f4421aacaa3306c023"' : 'data-target="#xs-components-links-module-PoProcessOutboxPageModule-12c39060409087f4421aacaa3306c023"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PoProcessOutboxPageModule-12c39060409087f4421aacaa3306c023"' :
                                            'id="xs-components-links-module-PoProcessOutboxPageModule-12c39060409087f4421aacaa3306c023"' }>
                                            <li class="link">
                                                <a href="components/PoProcessOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PoProcessOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PoProcessWorkItemPageModule.html" data-type="entity-link">PoProcessWorkItemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PoProcessWorkItemPageModule-a783fd6c4e67746059f8375551928e1c"' : 'data-target="#xs-components-links-module-PoProcessWorkItemPageModule-a783fd6c4e67746059f8375551928e1c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PoProcessWorkItemPageModule-a783fd6c4e67746059f8375551928e1c"' :
                                            'id="xs-components-links-module-PoProcessWorkItemPageModule-a783fd6c4e67746059f8375551928e1c"' }>
                                            <li class="link">
                                                <a href="components/PoProcessWorkItemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PoProcessWorkItemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PoProcessWorkItemsPageModule.html" data-type="entity-link">PoProcessWorkItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PoProcessWorkItemsPageModule-a0dafdd1b7791a2471d8e79ffbb1d1ef"' : 'data-target="#xs-components-links-module-PoProcessWorkItemsPageModule-a0dafdd1b7791a2471d8e79ffbb1d1ef"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PoProcessWorkItemsPageModule-a0dafdd1b7791a2471d8e79ffbb1d1ef"' :
                                            'id="xs-components-links-module-PoProcessWorkItemsPageModule-a0dafdd1b7791a2471d8e79ffbb1d1ef"' }>
                                            <li class="link">
                                                <a href="components/PoProcessWorkItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PoProcessWorkItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PreviewDocumentPageModule.html" data-type="entity-link">PreviewDocumentPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PreviewDocumentPageModule-9444f4b2c02e7efa22d1eaeba4dac314"' : 'data-target="#xs-components-links-module-PreviewDocumentPageModule-9444f4b2c02e7efa22d1eaeba4dac314"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PreviewDocumentPageModule-9444f4b2c02e7efa22d1eaeba4dac314"' :
                                            'id="xs-components-links-module-PreviewDocumentPageModule-9444f4b2c02e7efa22d1eaeba4dac314"' }>
                                            <li class="link">
                                                <a href="components/PreviewDocumentPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PreviewDocumentPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrProcessInboxPageModule.html" data-type="entity-link">PrProcessInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PrProcessInboxPageModule-f5645a4396ce0ea5f8614ea27aca289a"' : 'data-target="#xs-components-links-module-PrProcessInboxPageModule-f5645a4396ce0ea5f8614ea27aca289a"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrProcessInboxPageModule-f5645a4396ce0ea5f8614ea27aca289a"' :
                                            'id="xs-components-links-module-PrProcessInboxPageModule-f5645a4396ce0ea5f8614ea27aca289a"' }>
                                            <li class="link">
                                                <a href="components/PrProcessInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrProcessInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrProcessOutboxPageModule.html" data-type="entity-link">PrProcessOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PrProcessOutboxPageModule-4ac40fd94eec053f0c1f857c4a739e57"' : 'data-target="#xs-components-links-module-PrProcessOutboxPageModule-4ac40fd94eec053f0c1f857c4a739e57"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrProcessOutboxPageModule-4ac40fd94eec053f0c1f857c4a739e57"' :
                                            'id="xs-components-links-module-PrProcessOutboxPageModule-4ac40fd94eec053f0c1f857c4a739e57"' }>
                                            <li class="link">
                                                <a href="components/PrProcessOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrProcessOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrProcessWorkItemPageModule.html" data-type="entity-link">PrProcessWorkItemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PrProcessWorkItemPageModule-8912523d049c94f784b4b05fb54cd607"' : 'data-target="#xs-components-links-module-PrProcessWorkItemPageModule-8912523d049c94f784b4b05fb54cd607"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrProcessWorkItemPageModule-8912523d049c94f784b4b05fb54cd607"' :
                                            'id="xs-components-links-module-PrProcessWorkItemPageModule-8912523d049c94f784b4b05fb54cd607"' }>
                                            <li class="link">
                                                <a href="components/PrProcessWorkItemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrProcessWorkItemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PrProcessWorkItemsPageModule.html" data-type="entity-link">PrProcessWorkItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PrProcessWorkItemsPageModule-1c7e7f07193f80dcc7037d6a89cebd02"' : 'data-target="#xs-components-links-module-PrProcessWorkItemsPageModule-1c7e7f07193f80dcc7037d6a89cebd02"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PrProcessWorkItemsPageModule-1c7e7f07193f80dcc7037d6a89cebd02"' :
                                            'id="xs-components-links-module-PrProcessWorkItemsPageModule-1c7e7f07193f80dcc7037d6a89cebd02"' }>
                                            <li class="link">
                                                <a href="components/PrProcessWorkItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PrProcessWorkItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/RequestsPageModule.html" data-type="entity-link">RequestsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-RequestsPageModule-6c835217852090b1ea68b414438dfa4f"' : 'data-target="#xs-components-links-module-RequestsPageModule-6c835217852090b1ea68b414438dfa4f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-RequestsPageModule-6c835217852090b1ea68b414438dfa4f"' :
                                            'id="xs-components-links-module-RequestsPageModule-6c835217852090b1ea68b414438dfa4f"' }>
                                            <li class="link">
                                                <a href="components/RequestsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RequestsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SaProcessInboxPageModule.html" data-type="entity-link">SaProcessInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SaProcessInboxPageModule-ca1b6e3f81edee2c7352686897731aae"' : 'data-target="#xs-components-links-module-SaProcessInboxPageModule-ca1b6e3f81edee2c7352686897731aae"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SaProcessInboxPageModule-ca1b6e3f81edee2c7352686897731aae"' :
                                            'id="xs-components-links-module-SaProcessInboxPageModule-ca1b6e3f81edee2c7352686897731aae"' }>
                                            <li class="link">
                                                <a href="components/SaProcessInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SaProcessInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SaProcessOutboxPageModule.html" data-type="entity-link">SaProcessOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SaProcessOutboxPageModule-a4a2ce9d1624e2be1f7f412194167fb1"' : 'data-target="#xs-components-links-module-SaProcessOutboxPageModule-a4a2ce9d1624e2be1f7f412194167fb1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SaProcessOutboxPageModule-a4a2ce9d1624e2be1f7f412194167fb1"' :
                                            'id="xs-components-links-module-SaProcessOutboxPageModule-a4a2ce9d1624e2be1f7f412194167fb1"' }>
                                            <li class="link">
                                                <a href="components/SaProcessOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SaProcessOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SaProcessWorkItemPageModule.html" data-type="entity-link">SaProcessWorkItemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SaProcessWorkItemPageModule-72b82fc42786eeb41b6899ea4300c32d"' : 'data-target="#xs-components-links-module-SaProcessWorkItemPageModule-72b82fc42786eeb41b6899ea4300c32d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SaProcessWorkItemPageModule-72b82fc42786eeb41b6899ea4300c32d"' :
                                            'id="xs-components-links-module-SaProcessWorkItemPageModule-72b82fc42786eeb41b6899ea4300c32d"' }>
                                            <li class="link">
                                                <a href="components/SaProcessWorkItemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SaProcessWorkItemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SaProcessWorkItemsPageModule.html" data-type="entity-link">SaProcessWorkItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SaProcessWorkItemsPageModule-1367c38b225e07d17b0b71a1aedaa5d1"' : 'data-target="#xs-components-links-module-SaProcessWorkItemsPageModule-1367c38b225e07d17b0b71a1aedaa5d1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SaProcessWorkItemsPageModule-1367c38b225e07d17b0b71a1aedaa5d1"' :
                                            'id="xs-components-links-module-SaProcessWorkItemsPageModule-1367c38b225e07d17b0b71a1aedaa5d1"' }>
                                            <li class="link">
                                                <a href="components/SaProcessWorkItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SaProcessWorkItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SearchEngineerPageModule.html" data-type="entity-link">SearchEngineerPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SearchEngineerPageModule-aa79027dbe8e991fa6165b90a2ba6f8f"' : 'data-target="#xs-components-links-module-SearchEngineerPageModule-aa79027dbe8e991fa6165b90a2ba6f8f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SearchEngineerPageModule-aa79027dbe8e991fa6165b90a2ba6f8f"' :
                                            'id="xs-components-links-module-SearchEngineerPageModule-aa79027dbe8e991fa6165b90a2ba6f8f"' }>
                                            <li class="link">
                                                <a href="components/SearchEngineerPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchEngineerPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SearchTasklistPageModule.html" data-type="entity-link">SearchTasklistPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SearchTasklistPageModule-40cab79c13f2e0f26a6e0423cf02b6e7"' : 'data-target="#xs-components-links-module-SearchTasklistPageModule-40cab79c13f2e0f26a6e0423cf02b6e7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SearchTasklistPageModule-40cab79c13f2e0f26a6e0423cf02b6e7"' :
                                            'id="xs-components-links-module-SearchTasklistPageModule-40cab79c13f2e0f26a6e0423cf02b6e7"' }>
                                            <li class="link">
                                                <a href="components/SearchTasklistPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SearchTasklistPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiceEntryInboxPageModule.html" data-type="entity-link">ServiceEntryInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServiceEntryInboxPageModule-f82548de823659baeb7a063581c12641"' : 'data-target="#xs-components-links-module-ServiceEntryInboxPageModule-f82548de823659baeb7a063581c12641"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServiceEntryInboxPageModule-f82548de823659baeb7a063581c12641"' :
                                            'id="xs-components-links-module-ServiceEntryInboxPageModule-f82548de823659baeb7a063581c12641"' }>
                                            <li class="link">
                                                <a href="components/ServiceEntryInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServiceEntryInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiceEntryOutboxPageModule.html" data-type="entity-link">ServiceEntryOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServiceEntryOutboxPageModule-a58a5ada2f1197b22fc0c651579877f8"' : 'data-target="#xs-components-links-module-ServiceEntryOutboxPageModule-a58a5ada2f1197b22fc0c651579877f8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServiceEntryOutboxPageModule-a58a5ada2f1197b22fc0c651579877f8"' :
                                            'id="xs-components-links-module-ServiceEntryOutboxPageModule-a58a5ada2f1197b22fc0c651579877f8"' }>
                                            <li class="link">
                                                <a href="components/ServiceEntryOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServiceEntryOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiceEntryWorkItemPageModule.html" data-type="entity-link">ServiceEntryWorkItemPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServiceEntryWorkItemPageModule-1b62c3465c836ddc07a885baddec8aff"' : 'data-target="#xs-components-links-module-ServiceEntryWorkItemPageModule-1b62c3465c836ddc07a885baddec8aff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServiceEntryWorkItemPageModule-1b62c3465c836ddc07a885baddec8aff"' :
                                            'id="xs-components-links-module-ServiceEntryWorkItemPageModule-1b62c3465c836ddc07a885baddec8aff"' }>
                                            <li class="link">
                                                <a href="components/ServiceEntryWorkItemPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServiceEntryWorkItemPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiceEntryWorkItemsPageModule.html" data-type="entity-link">ServiceEntryWorkItemsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServiceEntryWorkItemsPageModule-974b1257ce350c4eb2af65ba3068c3ef"' : 'data-target="#xs-components-links-module-ServiceEntryWorkItemsPageModule-974b1257ce350c4eb2af65ba3068c3ef"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServiceEntryWorkItemsPageModule-974b1257ce350c4eb2af65ba3068c3ef"' :
                                            'id="xs-components-links-module-ServiceEntryWorkItemsPageModule-974b1257ce350c4eb2af65ba3068c3ef"' }>
                                            <li class="link">
                                                <a href="components/ServiceEntryWorkItemsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ServiceEntryWorkItemsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SettingsPageModule.html" data-type="entity-link">SettingsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SettingsPageModule-8adaa23f93df9d713f1e8bfc5ee778a6"' : 'data-target="#xs-components-links-module-SettingsPageModule-8adaa23f93df9d713f1e8bfc5ee778a6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SettingsPageModule-8adaa23f93df9d713f1e8bfc5ee778a6"' :
                                            'id="xs-components-links-module-SettingsPageModule-8adaa23f93df9d713f1e8bfc5ee778a6"' }>
                                            <li class="link">
                                                <a href="components/SettingsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SettingsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SorClaimOutboxPageModule.html" data-type="entity-link">SorClaimOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SorClaimOutboxPageModule-87bce15eabf43df94f484b362bcab6a3"' : 'data-target="#xs-components-links-module-SorClaimOutboxPageModule-87bce15eabf43df94f484b362bcab6a3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SorClaimOutboxPageModule-87bce15eabf43df94f484b362bcab6a3"' :
                                            'id="xs-components-links-module-SorClaimOutboxPageModule-87bce15eabf43df94f484b362bcab6a3"' }>
                                            <li class="link">
                                                <a href="components/SorClaimOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SorClaimOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SorClaimPageModule.html" data-type="entity-link">SorClaimPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SorClaimPageModule-6b61a0ab47b335fa0beada32978b6642"' : 'data-target="#xs-components-links-module-SorClaimPageModule-6b61a0ab47b335fa0beada32978b6642"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SorClaimPageModule-6b61a0ab47b335fa0beada32978b6642"' :
                                            'id="xs-components-links-module-SorClaimPageModule-6b61a0ab47b335fa0beada32978b6642"' }>
                                            <li class="link">
                                                <a href="components/SorClaimPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SorClaimPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SorInboxPageModule.html" data-type="entity-link">SorInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SorInboxPageModule-1fd32a131e3fa62744a4b78427ce69d9"' : 'data-target="#xs-components-links-module-SorInboxPageModule-1fd32a131e3fa62744a4b78427ce69d9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SorInboxPageModule-1fd32a131e3fa62744a4b78427ce69d9"' :
                                            'id="xs-components-links-module-SorInboxPageModule-1fd32a131e3fa62744a4b78427ce69d9"' }>
                                            <li class="link">
                                                <a href="components/SorInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SorInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SubmissionModalPageModule.html" data-type="entity-link">SubmissionModalPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SubmissionModalPageModule-306ef62fc850d3e9cf4415c2c670ac0e"' : 'data-target="#xs-components-links-module-SubmissionModalPageModule-306ef62fc850d3e9cf4415c2c670ac0e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SubmissionModalPageModule-306ef62fc850d3e9cf4415c2c670ac0e"' :
                                            'id="xs-components-links-module-SubmissionModalPageModule-306ef62fc850d3e9cf4415c2c670ac0e"' }>
                                            <li class="link">
                                                <a href="components/SubmissionModalPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SubmissionModalPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabsPageModule.html" data-type="entity-link">TabsPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TabsPageModule-3fc168b48b507f31c41bdf7e604f2c07"' : 'data-target="#xs-components-links-module-TabsPageModule-3fc168b48b507f31c41bdf7e604f2c07"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TabsPageModule-3fc168b48b507f31c41bdf7e604f2c07"' :
                                            'id="xs-components-links-module-TabsPageModule-3fc168b48b507f31c41bdf7e604f2c07"' }>
                                            <li class="link">
                                                <a href="components/TabsPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabsPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TabsRoutingModule.html" data-type="entity-link">TabsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/TimeclaimInboxPageModule.html" data-type="entity-link">TimeclaimInboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TimeclaimInboxPageModule-d7980a90ad43f37670e529bea233068b"' : 'data-target="#xs-components-links-module-TimeclaimInboxPageModule-d7980a90ad43f37670e529bea233068b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TimeclaimInboxPageModule-d7980a90ad43f37670e529bea233068b"' :
                                            'id="xs-components-links-module-TimeclaimInboxPageModule-d7980a90ad43f37670e529bea233068b"' }>
                                            <li class="link">
                                                <a href="components/TimeclaimInboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimeclaimInboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TimesheetClaimOutboxPageModule.html" data-type="entity-link">TimesheetClaimOutboxPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TimesheetClaimOutboxPageModule-22c078fed37025c954a76779bd20bc8b"' : 'data-target="#xs-components-links-module-TimesheetClaimOutboxPageModule-22c078fed37025c954a76779bd20bc8b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TimesheetClaimOutboxPageModule-22c078fed37025c954a76779bd20bc8b"' :
                                            'id="xs-components-links-module-TimesheetClaimOutboxPageModule-22c078fed37025c954a76779bd20bc8b"' }>
                                            <li class="link">
                                                <a href="components/TimesheetClaimOutboxPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimesheetClaimOutboxPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TimesheetClaimPageModule.html" data-type="entity-link">TimesheetClaimPageModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TimesheetClaimPageModule-2ce4f0f4c744836ebc924994cc9b7bd3"' : 'data-target="#xs-components-links-module-TimesheetClaimPageModule-2ce4f0f4c744836ebc924994cc9b7bd3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TimesheetClaimPageModule-2ce4f0f4c744836ebc924994cc9b7bd3"' :
                                            'id="xs-components-links-module-TimesheetClaimPageModule-2ce4f0f4c744836ebc924994cc9b7bd3"' }>
                                            <li class="link">
                                                <a href="components/TimesheetClaimPage.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TimesheetClaimPage</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/GlobalService.html" data-type="entity-link">GlobalService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/InboxService.html" data-type="entity-link">InboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LoginService.html" data-type="entity-link">LoginService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NetworkService.html" data-type="entity-link">NetworkService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/OutboxService.html" data-type="entity-link">OutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PoProcessInboxService.html" data-type="entity-link">PoProcessInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PoProcessOutboxService.html" data-type="entity-link">PoProcessOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PoProcessWorkItemService.html" data-type="entity-link">PoProcessWorkItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PoProcessWorkItemsService.html" data-type="entity-link">PoProcessWorkItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PrProcessInboxService.html" data-type="entity-link">PrProcessInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PrProcessOutboxService.html" data-type="entity-link">PrProcessOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PrProcessWorkItemService.html" data-type="entity-link">PrProcessWorkItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PrProcessWorkItemsService.html" data-type="entity-link">PrProcessWorkItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RequestsService.html" data-type="entity-link">RequestsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RestcallsService.html" data-type="entity-link">RestcallsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SaProcessInboxService.html" data-type="entity-link">SaProcessInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SaProcessOutboxService.html" data-type="entity-link">SaProcessOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SaProcessWorkItemService.html" data-type="entity-link">SaProcessWorkItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SaProcessWorkItemsService.html" data-type="entity-link">SaProcessWorkItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServiceEntryInboxService.html" data-type="entity-link">ServiceEntryInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServiceEntryOutboxService.html" data-type="entity-link">ServiceEntryOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServiceEntryWorkItemService.html" data-type="entity-link">ServiceEntryWorkItemService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ServiceEntryWorkItemsService.html" data-type="entity-link">ServiceEntryWorkItemsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SettingsService.html" data-type="entity-link">SettingsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SorClaimOutboxService.html" data-type="entity-link">SorClaimOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SorClaimService.html" data-type="entity-link">SorClaimService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/SorInboxService.html" data-type="entity-link">SorInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StorageService.html" data-type="entity-link">StorageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TabsService.html" data-type="entity-link">TabsService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimeclaimInboxService.html" data-type="entity-link">TimeclaimInboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimesheetClaimOutboxService.html" data-type="entity-link">TimesheetClaimOutboxService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TimesheetClaimService.html" data-type="entity-link">TimesheetClaimService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/JwtInterceptor.html" data-type="entity-link">JwtInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuardService.html" data-type="entity-link">AuthGuardService</a>
                            </li>
                            <li class="link">
                                <a href="guards/RoleGuardService.html" data-type="entity-link">RoleGuardService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});